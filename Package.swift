// swift-tools-version: 5.9
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MyPackage",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        // Products define the executables and libraries a package produces, making them visible to other packages.
        .library(
            name: "MyPackage",
            targets: ["MyPackage"]),
    ],
    targets: [
        // Targets are the basic building blocks of a package, defining a module or a test suite.
        // Targets can depend on other targets in this package and products from dependencies.
        .target(
            name: "MyPackage",
            dependencies: ["MediaPipeTasksCommon", "MediaPipeTasksVision"],
            cSettings: [
               .unsafeFlags(["-w"])
            ],
            swiftSettings: [
                .unsafeFlags(["-suppress-warnings"]),
            ]
        ),
        .binaryTarget(name: "MediaPipeTasksCommon", path: "MediaPipe/MediaPipeTasksCommon.xcframework"),
        .binaryTarget(name: "MediaPipeTasksVision", path: "MediaPipe/MediaPipeTasksVision.xcframework"),
        .testTarget(
            name: "MyPackageTests",
            dependencies: ["MyPackage"]),
    ]
)
